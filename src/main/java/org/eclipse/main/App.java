package org.eclipse.main;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.model.Adresse;
import org.eclipse.model.Personne;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class App 
{
    public static void main( String[] args )
    {
    	
    	Adresse adresse1 = new Adresse(); 
    	adresse1.setRue("berger"); 
    	adresse1.setCodePostal("14015"); 
    	adresse1.setVille("Menton");
    	
    	Adresse adresse2 = new Adresse(); 
    	adresse2.setRue("gerard"); 
    	adresse2.setCodePostal("13037"); 
    	adresse2.setVille("Valbonne");
    	
    	Adresse adresse3 = new Adresse(); 
    	adresse3.setRue("popo"); 
    	adresse3.setCodePostal("20035"); 
    	adresse3.setVille("Nissa");
    	
    	Personne personne = new Personne();  
    	personne.setId(1);
    	personne.setNom("nnono"); 
    	personne.setPrenom("Pidra");
    	personne.addAdresse(adresse1);
    	personne.addAdresse(adresse2);
    	personne.addAdresse(adresse3);
    	
    	
    	   	
    	Configuration configuration = new Configuration().configure(); 
    	SessionFactory sessionFactory = configuration.buildSessionFactory(); 
    	Session session = sessionFactory.openSession(); 
    	Transaction transaction = session.beginTransaction(); 
    	
    	Integer cle = (Integer) session.save(personne); 
    	
    	System.out.println(cle);
    	
    	//personne.setId(5);
    	
    	session.persist(personne); 
    	
    	transaction.commit(); 
    	session.close(); 
    	sessionFactory.close();

    }
}
